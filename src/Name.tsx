import styled from "styled-components";
import { animated, useSpring, config } from "react-spring";

const NameContainer = styled(animated.div)`
  transition: all 0.5s ease-in-out;
  font-size: 2.5rem;
  color: #eef;
  position: absolute;
  top: 0;
  left: 15%;
  font-family: "VT323", monospace;
`;

const SubTitle = styled.div`
  font-size: 0.5em;
  color: white;
`;

const Name = () => {
  const styles = useSpring({
    config: config.stiff,
    to: { opacity: 1, transform: `translate3d(0,50%,0)` },
    from: {
      opacity: 0,
      transform: `translate3d(-300%,50%,0)`,
    },
  });

  return (
    <NameContainer style={styles}>
      Manuel Maceira
      <SubTitle>FullStack Developer</SubTitle>
    </NameContainer>
  );
};

export default Name;
