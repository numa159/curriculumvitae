import styled from "styled-components";
import Name from "./Name";
import Skeletor from "./Skeletor";

const StyledApp = styled.div`
  background: linear-gradient(112.1deg, #001 11.4%, #023 70.2%);
  width: 100vw;
  height: 100vh;
  position: relative;
  padding: 0;
  margin: 0;
  overflow: hidden;
`;

const App = () => (
  <StyledApp>
    <Skeletor color="#0002" />
    <Name />
  </StyledApp>
);
export default App;
